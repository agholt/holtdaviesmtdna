# HoltDaviesMtDNA

JupyterLab notebooks: supporting data and analysis code for paper:

The effect of mitochondrial DNA half-life on clonal expansion in long lived cells The potential for delaying the onset of age related neurodegenerative diseases

msim.py: simulator code

Dockerfile: Dockerfile

build_images.sh: Script to build docker image
        
Alan Holt and Adrian Davies
