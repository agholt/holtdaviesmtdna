#!/bin/bash

# msim launch script
# (c) Alan Holt 2018

set -e

# Configure these parameters:
COUNT=2
DESC="2test"
PARAM_FILE="msim-1y.json"

DDIR="/opt/Research/Msim/" # dir for results

IMAGE=msim		
tag=latest
container=${IMAGE}

OPTS[0]="-c ${COUNT}"
OPTS[1]="--desc=${DESC}"
OPTS[2]="-p ${DDIR}/${PARAM_FILE}"

docker run -u mito -v `pwd`/opt:/opt --rm -d ${container}:${tag}  ${OPTS[*]}

