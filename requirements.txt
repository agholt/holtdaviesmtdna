biopython==1.72
jsonschema==2.6.0
numpy==1.15.2
pyparsing==2.2.2
python-dateutil==2.7.3
six==1.11.0
