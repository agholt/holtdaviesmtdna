#!/usr/bin/env python

__author__ = "Alan Holt"
__copyright__ = "Copyright 2019"
__credits__ = ["Adrian Davies"]
__license__ = "Proprietary"
__version__ = "1.0.2"
__maintainer__ = "Alan Holt"
__email__ = "agholt@gmail.com"
__status__ = "Production"


from Bio.Seq import Seq
from Bio import SeqIO
from Bio import SeqUtils
from Bio import Entrez
from Bio.Alphabet import IUPAC
from Bio.Data import CodonTable


import itertools
import datetime
import json
from io import StringIO
import hashlib
import timeit
import csv
import sys


from optparse import OptionParser


import random
import numpy as np


def proc_options():
    parser = OptionParser()
    parser.add_option('-c', '--count', dest='count',
                      help = 'specify number of repetitions')
    parser.add_option('-d', '--desc', dest='desc',
                      help = 'specify description')
    parser.add_option('-p', '--params', dest='paramfile',
                      help = 'specify parameter file')

    return parser.parse_args(sys.argv[1:])



def fission(m_dct,s_thresh):
    
    # shuffle
    m_lst = [(k,v) for k,v in m_dct.iteritems()]
    np.random.shuffle(m_lst)

    # split
    m = m_lst
    n = len(m_lst)/s_thresh
    m2_lst = []
    for i in xrange(n):
        
        x,rest = m[:s_thresh], m[s_thresh:]
        x_dct = conv_lst_to_dct(x)
        m2_lst.append(x_dct)
        m = rest
        
    return m2_lst


# Class definitions


class CFn():
    
    def _get_param(self,param_key, default_val):
        
        if self.params.has_key(param_key):
            new_param = self.params[param_key]
        else:
            new_param = default_val
                                    
        return new_param
    
    def debug_output(self, msg, debug_level):
        
        if self.debug >= debug_level:
            ts = str(datetime.datetime.now()).split('.')[0].replace(' ', '_')
            print("%s: %s" % (ts,msg))
            
        return None


class ATP():
        
    def init_atp(self, atp_level):
        self.atp = atp_level
        self.atp_lst.append(self.atp)
        return self.atp

    def save_atp(self):
        self.atp_lst.append(self.atp)
        return self.atp
    
    def add_atp(self, atp_level):
        self.atp += atp_level
        return self.atp
    
    def del_atp(self, atp_level):
        consumed = min(self.atp, atp_level)
        self.atp -= atp_level
        self.atp = max(0,self.atp)
        return consumed
    
    def record_atp(self, atp_level, src, clone):
        
        self.atp_df['TIME'].append(self.t)
        self.atp_df['SRC'].append(src)
        self.atp_df['ATP'].append(atp_level)
        self.atp_df['CLONE'].append(clone)
        
        return None


class mtDNA(CFn):
    
    def __init__(self, mtseq, params, env):

        self.params = params
        
        # Initialise
        self.env = env
        self.ID = env.get_ID() 
        self.PID = 0
        self.busy = 0
        self.generation = 0
        self.ATP_FLAG = True
        
        self.set_mtseq(mtseq) # add mitochondrial sequence to database
        
        # Set parameters
        self._set_params()
        
        self.compute_max_busy()
        
        self.can_produce_apt()
        if self.atp_producer:
            self.env.atp_producer_lst.append((self.ID,self.type))
        
        # Warning this will generate a lot of messages if
        msg = "mtDNA:clone t: %s id: %s max_busy: %s type %s" % (self.env.t, self.ID, self.max_busy, self.type)
        self.debug_output(msg,10)
    
    # Set parameters
    def _set_params(self):
         
        # Wild type parameters
        self.wild_type = self._get_param('WILD_TYPE', None)
        self.wild_len = self._get_param('WILD_LEN', 1)
        
        self.busy_scale = float(self.sequence_len)/float(wild_len)
        
        # Busy and TTL paramaters
        self.ttl = self._get_param('MAX_TTL', 0)
        self.max_repl_time = self._get_param('MAX_REPL_TIME', 0)
        self.constant_repl_time = self._get_param('CONSTANT_REPL_TIME', False)
        
        # ATP produbction rate   
        self.produce_rate = self._get_param('ATP_PRODUCTION_RATE', 0)       # mtDNA ATP production
        self.repl_consume_rate = self._get_param('REPL_ATP_CONSUME_RATE', 0)
        self.atp_production_always_on  = self._get_param('ATP_PRODUCTION_ALWAYS_ON', True)
        self.allow_mutant_atp_producers  = self._get_param('ALLOW_MUTANT_ATP_PRODUCERS', False)
        
        # Event probabilities
        self.damage_prob = self._get_param('DAMAGE_PROB', 0.0)
        self.mutate_prob = self._get_param('MUTATE_PROB', 0.0)
           
        clone_probs = self._get_param('CLONE_PROBS', (0.0,0.0,0.0))
        self.p1, self.p2, self.p3 = clone_probs
           
        # Clone probability
        # replS, either used as static clone probability or as part of Verhulst pop. eqn.
        self.replS = self._get_param('CLONE_replS', 0)
        
        # Verhultst population growth
        self.verhulst_thresh = self._get_param('VERHULST_THRESH', 0.0)
        self.verhulst_scale = self._get_param('VERHULST_SCALE', 1)

        pop_threshs = self._get_param('POP_THRESH', (8,12)) 
        self.pop_thresh1, self.pop_thresh2 = map(int,pop_threshs)
        
        self.clone_prob_method = self._get_param('CLONE_PROB_METHOD', 'CONSTANT')
        
        self.debug = self._get_param('DEBUG_LEVEL', 0)
        self.log_to_file = self._get_param('LOG_TO_FILE', False)
        self.log_only_to_file = self._get_param('LOG_ONLY_TO_FILE', False)
        self.log_file = self._get_param('LOG_FILE', "msim.log")
  
    def display_params(self):
        
        print("wild_type: %s" % (self.wild_type))
        print("wild_len: %d" % (self.wild_len))
        print("ttl: %d" % (self.ttl))
        print("max_repl_time: %d" % (self.max_repl_time))
        print("max_busy: %d" % (self.max_busy))
        print("busy_scale: %f" % (self.busy_scale))
        
        print("produce_rate: %d" % (self.produce_rate))
        print("repl_consume_rate: %d" % (self.repl_consume_rate))
        print("atp_production_always_on: %s" % (self.atp_production_always_on))
        print("allow_mutant_atp_producers: %s" % (self.allow_mutant_atp_producers))
        
        print("damage_prob: %s" % (self.damage_prob))
        print("mutate_prob: %s" % (self.mutate_prob))
        
        print("replS: %s" % (self.replS))
        print("verhulst_thresh: %f" % (self.verhulst_thresh))
        print("verhulst_scale: %f" % (self.verhulst_scale))
        
        print("pop_thresh1: %d" % (self.pop_thresh1))
        print("pop_thresh2 %d" % (self.pop_thresh2))
        print("clone probabilities: %s %s %s" % (self.p1, self.p2, self.p3))
        print("clone_prob_method: %s" % (self.clone_prob_method))
        
        print("debug: %s" % (self.debug))
        print("log_to_file: %s" % (self.log_to_file))
        print("log_only_to_file: %s" % (self.log_only_file))
        print("log_file: %s" % (self.log_file))

    # Hash the sequence of the mtdna 
    def set_mtseq(self, mtseq):
        self.type = hashlib.sha224(str(mtseq)).hexdigest()
        self.sequence_len = len(mtseq)
        self.no_codons = int(self.sequence_len/3)
        # Add mtdna seq the mtseqs database
        if not self.env.mtseqs.has_key(self.type):
            self.env.mtseqs[self.type] = mtseq
        return None
    
    def can_produce_apt(self):
        
        self.atp_producer = False # initalise atp_producer to false
                                             
        # All wild types are ATP producers
        if self.type == wild_type:
            self.atp_producer = True
            return None
        
        # See if mutant can produce ATP
        if self.allow_mutant_atp_producers:   # if the feature is turned on
            S = self.env.mtseqs[self.type]
            protein = S.transcribe().translate(table="Vertebrate Mitochondrial")
                                             
            if self.env.atp_seq in protein:
                self.atp_producer = True
                    
                
        return None
        
    # Return the mtdna's ID
    def get_id(self):
        return self.ID
    
    # Set the mtdna's parent ID
    def set_parent(self, pid):
        self.PID = pid
        return self.PID
    
    # Return the parent ID of the mtdna
    def get_parent(self):
        return self.PID
    
    # start replication timer (called from clone method)
    def set_busy(self, busy):
        self.busy = busy
        return None
    
    # Set ATP generation flag (True of False)
    def set_atp(self, state):
        self.ATP_FLAG = state
        return None
    
    # Set time to live
    def set_ttl(self, ttl):
        self.ttl = ttl
        return None
    
    # damage has occured, decrement TTL  
    def dec_ttl(self):
        self.ttl -= 1
        if self.ttl < 0:
            self.ttl = 0
        return None
    
    # Decrement busy period
    def dec_busy(self):
        self.busy -= 1
        self.busy = max(0, self.busy)
    
    # Computer next generation
    def inc_generation(self,last_gen):
        self.generation = last_gen + 1
    
    def rnd_event(self, lbound,ubound, thresh):
        return np.random.randint(lbound, ubound) < thresh
    
    # Produce/consume ATP
    def produce_atp(self,atp_level):
        # If atp_production_always_on flag set, then override self.ATP_FLAG
        if self.atp_production_always_on:
            atp_flag = True
        else:
            atp_flag = self.ATP_FLAG
        
        if self.atp_producer and atp_flag:
            self.env.add_atp(atp_level)
            
        return None
    
    def consume_atp(self, atp_level):
        consumed = self.env.del_atp(atp_level)
        return consumed

    # Computes the mtdna'a busy period
    def compute_busy(self):
                
        if self.type == self.wild_type:
            return self.max_busy
        else: 
            # Replication time computed according to size of mtDNA
            new_busy = min(1, int(self.busy_scale * self.max_busy))
            return new_busy
      
        # Never gets here unless above lines commented out
        return self.max_busy
    
    # Computes the mtdna'a busy period
    def compute_max_busy(self):
        
        self.busy_scale = float(self.sequence_len)/float(self.wild_len)
        
        if self.type == self.wild_type or self.constant_repl_time:
            self.max_busy = self.max_repl_time   
        else: 
            # Replication time computed according to size of mtDNA
            self.max_busy = max(1, int(self.busy_scale * self.max_repl_time))

        return None
    
    # Calculate the status of the mtdna
    def calc_status(self):
        
        if self.busy < 0:
            consumed = self.consume_atp(self.repl_consume_rate)
        #    msg = "mtDNA:calc_status t: %s id: %s is cloning" % (self.env.t, self.ID)
        #    self.debug_output(msg,4)
            
        self.dec_busy() # calc busy time
        
        # Produce ATP  
        #self.produce_atp(self.produce_rate)

        if self.type == self.wild_type:
            self.env.add_atp(self.produce_rate)
        
        if random.uniform(0, 1) < self.damage_prob:
            self.dec_ttl()

    
    # mutate
    def mutate(self, mtseq):
        
        #seq_len = int(3*self.no_codons/2)
    
        # Take a fragment of the mitochondrial sequence
        frag_len = 3 * int(self.no_codons/2)
        
        if frag_len > 1000:
            offset = np.random.randint(0, self.sequence_len-1)
            rotated_seq = mtseq[offset:] + mtseq[:offset] 
            new_seq = rotated_seq[frag_len:]
        else:
            new_seq = mtseq
        
        msg = "mtDNA:mutate t: %s frag_len: %s" % (self.env.t, frag_len)
        self.debug_output(msg,4)
        
        return new_seq
    
    # Compute clone probability as a function of population size
    def get_clone_prob(self):
        
        # Constant cloning probability
        if self.clone_prob_method == 'CONSTANT':
            return self.replS
                
        # Clone probability based upon Verhulst dynamnics
        if self.clone_prob_method == 'VERHULST':
            return self.replS / (1.0 + float(self.verhulst_scale*env.count)/self.verhulst_thresh)
            
        # Piecewise equation
        if self.clone_prob_method == 'PIECEWISE':
            if env.count < self.pop_thresh1:
                return self.p1
            if env.count < self.pop_thresh2:
                return self.p2    
            return self.p3
            
        return self.replS 
    
    def update_env(self):

        if self.env.gen_logging_on:
            mparams = {
                'PID': self.PID,
                'TYPE': self.type,
                 'SEQ_LEN': self.sequence_len,
                 'ATP_PRODUCER': self.atp_producer,
                 'GEN': self.generation
            }
            self.env.add_mt(self.ID, mparams)
        
        return None
        
    # clone mtdna
    def clone(self):
        
        if self.ttl > 1 and self.busy == 0:
            
            clone_prob = self.get_clone_prob()
            
            if random.uniform(0, 1) < clone_prob and self.env.count < self.env.max_population:
                
                #self.busy = self.compute_busy() 
                self.busy = self.max_busy
                
                # Perform mutantion (possibly)
                new_seq = self.env.mtseqs[self.type]
                if random.uniform(0, 1) < self.mutate_prob:
                    new_seq = self.mutate(new_seq)           
                
                m_params = self.params
                m_clone = mtDNA(new_seq, m_params, self.env)      # clone
                if m_clone.type not in self.env.species_lst:
                    self.env.species_lst.append(m_clone.type)
                    
                m_clone.set_busy(self.max_busy)                   # set clone as busy
                m_clone.set_parent(self.ID)                   # set parent
                m_clone.inc_generation(self.generation)       # Put into next generation
                m_clone.update_env()
                
                return m_clone
            
        return None   


class Mito(CFn, ATP):
    
    def __init__(self, params):
                
        self.params = params
        self._set_params()
        self._init_data()
        
        if self.debug >= 2:
            self.display_params()
    
    def _set_params(self):
        
        self.CLONE_FLAG = 1
        
        self.max_population = self._get_param('MAX_POPULATION', 10)
        self.run_id = self._get_param('RUN_ID', 1)
        self.wild_type = self._get_param('WILD_TYPE', None)
        self.consume_rate = self._get_param('ATP_CONSUMPTION_RATE', 100)
        self.atp_seq = self._get_param('ATP_SEQ', '')
        self.sample_per_hour = self._get_param('SAMPLE_PER_HOUR', 0)
        self.debug = self._get_param('DEBUG_LEVEL', 0)
        self.gen_logging_on = self._get_param('GEN_LOGGING_ON', False)
        self.log_to_file = self._get_param('LOG_TO_FILE', False)
        self.log_file = self._get_param('LOG_FILE', "msim.log")
        self.log_only_to_file = self._get_param('LOG_ONLY_TO_FILE', False)
        self.terminate_on_extinction = self._get_param('TERMINATE_ON_EXTINCTION', False)
       
    
    def _init_data(self):
         
        # Wild type parameters   
        self.ID_count = 0
        #self.n = 0
        self.t = 0
        
        self.mtda_lst = []
        
        self.count = 0
        self.count_lst = []
        
        self.mtseqs = {}
        self.mtids = {}
        
        self.species_lst = []
        self.species_dct = {}
        
        self.no_species = []
        
        self.atp = 0
        self.atp_lst = []        
        self.atp_producer_lst = []
        
        self.m_state = []

    def display_params(self):
        
        print("max_population : %s" % (self.max_population))
        print("run_id : %s" % (self.run_id))
        print("wild_type: %s" % (self.wild_type))
        print("consume_rate: %s" % (self.consume_rate))
        print("atp_seq: %s" % (self.atp_seq))
        print("terminsate_on_extinction: %s" % (self.terminate_on_extinction))
        print("debug: %s" % (self.debug))
        print("gen_logging_on: %s" % (self.gen_logging_on))
        print("log_to_file: %s" % (self.log_to_file))
        print("log_only_to_file: %s" % (self.log_only_to_file))
        print("log_file: %s" % (self.log_file))
    
    def create_mtdna(self,attr_lst):
        self.mtdna_lst = []
        for attr in attr_lst:
            seq, params = attr
            #params = attr[1]
            m = mtDNA(seq,params,self)
            m.update_env()
            self.mtdna_lst.append(m)
            
        self.count_species()
            
        return None

    def get_ID(self):
        self.ID_count += 1
        return self.ID_count
        
    def add_mt (self, ID, param_dct):
        p = param_dct
        if not self.mtids.has_key(ID):
            p.update({'ID': ID})
            p.update({'tSTART': self.t})
            p.update({'tEND': 0})        
            self.mtids[ID] = p
    
    def set_mt_end_date(self, ID):
        if self.mtids.has_key(ID):
            self.mtids[ID]['tEND'] = self.t     
    
    def save_state(self, m_dct):
        self.m_state.append(m_dct)
    
    def init_pop_count(self, count):
        self.count = count
        self.count_lst.append(count)

    # Delete mtdna will TTL=0
    def delete_dead(self):
        i = 0
        for m in self.mtdna_lst:
            if m.ttl < 1:
                self.mtdna_lst.pop(i)
                self.set_mt_end_date(m.ID)
            i+=1
    
    # Count the numner of mtdna and the number of diiferent species
    def count_species(self):
        self.count = len(self.mtdna_lst)
        self.count_lst.append(self.count)
        
        species = [m.type for m in self.mtdna_lst]
        s_count = [(s, species.count(s)) for s in set(species)]
        pop_count = dict(s_count)
        self.no_species.append(len(s_count))
        
        for mtype in pop_count:
            count = pop_count[mtype]
            if self.species_dct.has_key(mtype):
                self.species_dct[mtype][1].append(count)
            else:    
                self.species_dct[mtype] = (self.t, [count])
                
        # self.species_count(m_dct)   
    
    # Run the simaultion
    def run_sim(self, run_length):
           
        t1 = timeit.default_timer()
        
        for t in xrange(run_length):
            
            self.t += 1
            self.save_atp()
        
            
            if t % (24 * self.sample_per_hour * 50) == 0:
                day = int(t/float(24 * self.sample_per_hour))
                msg = "Mito:run_sim days: %d total pop: %d no. species %d" % (day, self.count, self.no_species[-1])
                self.debug_output(msg,1)
            
            # Purge dead mtdna
            self.delete_dead()

            consumed = self.del_atp(self.consume_rate)  
        
            child_lst = []
            wild_type_flag = False
            
            for m in self.mtdna_lst:
                
                if m.type == wild_type:
                    wild_type_flag = True
                    
                # Turn ATP production on/off
                if consumed < self.consume_rate:
                    m.set_atp(True)
                else:
                    m.set_atp(False)
                    
                # Age and reduce process busy period (if in busy period)    
                m.calc_status()
        
                # Clone
                if consumed < self.consume_rate:     
                    child = m.clone()
                    if child != None:
                        self.mtdna_lst.append(child)
          
            self.count_species()
            
            #if self.species_dct[wild_type][1][-1] == 1:
            if not wild_type_flag:
                state = {}
                state['DESCRIPTION'] = "wild type extinction"
                state['TIME'] = self.t
                state['RUN_LENGTH'] = run_length
                state['SAMPLE_PER_HOUR'] = self.sample_per_hour
                state['ATP'] = self.atp_lst[-1]
                state['NO_MTDNA'] = self.count_lst[-1]
                state['NO_WILD_TYPE'] = self.species_dct[wild_type][1][-1]
                state['NO_SPECIES'] = self.no_species[-1]
                state['TOTAL_SPECIES'] = len(set(self.species_dct.keys()))
                state['MUTATE_PROB'] = self.mtdna_lst[-1].mutate_prob
                state['RUN_ID'] = self.run_id
                state['DATETIME'] = str(datetime.datetime.now()).split('.')[0].replace(' ', '_')

                self.save_state(state)
                
                if self.terminate_on_extinction:
                    t2 = timeit.default_timer()
                    return t1,t2




        state = {}
        state['DESCRIPTION'] = "end of simulation"
        state['TIME'] = self.t
        state['RUN_LENGTH'] = run_length
        state['SAMPLE_PER_HOUR'] = self.sample_per_hour
        state['ATP'] = self.atp_lst[-1]
        state['NO_MTDNA'] = self.count_lst[-1]
        state['NO_WILD_TYPE'] = self.species_dct[wild_type][1][-1]
        state['NO_SPECIES'] = self.no_species[-1]
        state['TOTAL_SPECIES'] = len(set(self.species_dct.keys()))
        state['MUTATE_PROB'] = self.mtdna_lst[-1].mutate_prob
        state['RUN_ID'] = self.run_id
        state['DATETIME'] = str(datetime.datetime.now()).split('.')[0].replace(' ', '_')
        self.save_state(state)    
        print(state)

        t2 = timeit.default_timer()
        return (t1,t2)



# Write timeseries data
def write_ts(O, ts, res_dir):
    ts_file = "%s/mito-ts-%s_%s.json" % (res_dir,desc,ts)
    #ts_file = "mito-ts.json"
    
    df = {"META": {}, "DATA": {}}
    df['META'] = {'wild_type': wild_type, 'run_length': run_length, 'samples_per_day': 96}
    df['DATA'] = O.species_dct
    df['TOTAL'] = O.count_lst
    df['NO_SPECIES'] = O.no_species
    df['ATP'] = O.atp_lst
    
    with open(ts_file, 'w') as fd:
        fd.writelines(json.dumps(df))
    print("Timeseries results written to: %s" % (ts_file))


# Write generation data
def write_gen(O, ts, res_dir):
    gen_file = "%s/mito-gen-%s_%s.json" % (res_dir, desc,ts)
    #gen_file = "mito-gen.json"
    
    mtids_lst = [O.mtids[k] for k in O.mtids]
    df = {"META": {}, "DATA": {}}
    df['META'] = {'wild_type': wild_type, 'run_length': run_length, 'samples_per_day': sample_per_hour * 24}
    df['DATA'] = O.mtids
  
    with open(gen_file, 'w') as fd:
        fd.writelines(json.dumps(df))
    print("Generation results written to: %s" % (gen_file))



if __name__== '__main__':


    print("Author: %s:"  % (__author__))
    print("Credits: %s:" % (__credits__))
    print("Version: %s:" % (__version__))
    print("Status %s:"   % (__status__))
    print("Copyright %s:" % (__copyright__))


    options,files = proc_options()

    if options.paramfile != None:
        msim_file = options.paramfile
    else:
        msim_file = "msim.json"

    if options.count != None:
        count = int(options.count)
    else:
        count = 1

    if options.desc != None:
        desc = options.desc
    else:
        desc = "test"

    with open(msim_file, 'rU') as fd:
        params = json.load(fd)


    meta_params = params['META']
    mito_params = params['MITO_PARAMS']
    mtdna_params = params['MTDNA_PARAMS']

    results_dir = meta_params['DIR']
    sample_per_hour = mito_params['SAMPLE_PER_HOUR']

    no_days = int(meta_params['NO_DAYS'])
    no_years = int(meta_params['NO_YEARS'])
    run_length = 24 * sample_per_hour * no_days * no_years ; run_length

    wild_seq = Seq(params['WILD_SEQ'])
    wild_len = len(wild_seq); wild_len

    wild_type = hashlib.sha224(str(wild_seq)).hexdigest() ; wild_type

    init_pop_size = meta_params['INIT_POP_SIZE']

    ts = str(datetime.datetime.now()).split('.')[0].replace(' ', '_')
    log_file = "msim-%s_%s.log" % (desc, ts); log_file
    mito_params['LOGFILE'] = log_file
    mtdna_params['LOGFILE'] = log_file


    results_file = "%s/mito-results-%s_%s.json" % (results_dir,desc,ts)
    results = []

    for i in range(count):
        mito_params['RUN_ID'] = i
        O = Mito(mito_params)
        attr_lst = [(wild_seq, mtdna_params)] * init_pop_size
        O.create_mtdna(attr_lst)
        for m in O.mtdna_lst:
            m.set_ttl(np.random.randint(1, 10))
        t1,t2 = O.run_sim(run_length)
        print('Run time: ', t2 - t1)

        results.append(O.m_state)

        ts = str(datetime.datetime.now()).split('.')[0].replace(' ', '_')
        write_ts(O,ts,results_dir)
        write_gen(O,ts,results_dir)


    for r in results:
        print r

    with open(results_file, 'w') as fd:
        fd.writelines(json.dumps(results))
    print("Results written to: %s" % (results_file))

    print("%s" % (str(datetime.datetime.now()).split('.')[0].replace(' ', '_')))

