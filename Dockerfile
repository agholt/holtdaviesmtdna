FROM python:2.7-alpine as msim-build

RUN apk add --update \
    gcc \
    musl-dev \
    linux-headers

COPY requirements.txt .
RUN pip install -r requirements.txt

FROM python:2.7-alpine as msim

COPY --from=msim-build /root/.cache /root/.cache
COPY --from=msim-build requirements.txt .
RUN pip install -r requirements.txt && rm -rf /root/.cache

RUN addgroup -g 1024 -S mito
RUN adduser -S -G mito -u 1023 -h /home/mito mito
USER mito

WORKDIR /home/mito
COPY msim.py /home/mito

ENTRYPOINT ["/home/mito/msim.py"]
CMD ["-c 1 -desc=test -p /opt/Research/Msim/msim-100d.json"]

